# ics-ans-role-zabbix-agent-del

Ansible role to remove zabbix agent.

## Role Variables

```yaml
...
# None
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zabbix-agent-del
```

## License

BSD 2-clause
