import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_zabbix_agent_not_running(host):
    zabbix = host.service("zabbix-agent")
    assert zabbix.is_enabled is False
    assert zabbix.is_running is False


def test_zabbix_dir_is_missing(host):
    conf_path = host.file("/etc/zabbix")
    assert conf_path.exists is False
